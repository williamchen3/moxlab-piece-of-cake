using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabObjectRegister : MonoBehaviour
{
    [HideInInspector]
    public Material MaterialBall;

    [HideInInspector]
    public int Order;

    [HideInInspector]
    public ColorTypes ColorType;

    [HideInInspector]
    public GrabTypes GrabType;

    // Start is called before the first frame update
    public void Init()
    {
        Color mColor;    

        switch (ColorType)
        {
            case ColorTypes.Red:
                ColorUtility.TryParseHtmlString(GameController.RedColorValue, out mColor);                
                break;
            case ColorTypes.Tiffany:
                ColorUtility.TryParseHtmlString(GameController.TiffanyColorValue, out mColor);
                break;
            case ColorTypes.Ivory:
                ColorUtility.TryParseHtmlString(GameController.IvoryColorValue, out mColor);
                break;
            case ColorTypes.Blue:
                ColorUtility.TryParseHtmlString(GameController.BlueColorValue, out mColor);
                break;
            case ColorTypes.Orange:
                ColorUtility.TryParseHtmlString(GameController.OrangeColorValue, out mColor);
                break;
            default:
                ColorUtility.TryParseHtmlString(GameController.RedColorValue, out mColor);
                break;
        }

        MaterialBall.color = mColor;
        this.GetComponent<MeshRenderer>().material = MaterialBall;
    }


}
