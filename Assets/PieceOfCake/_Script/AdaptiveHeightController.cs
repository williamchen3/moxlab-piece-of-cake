using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

//AdaptiveHeightController 功能說明:
//場景物建會自動適應眼部高度

public class AdaptiveHeightController : MonoBehaviour
{

    [Header("GameObject")]
    public GameObject AdaptiveHeightContainer;

    [Header("public setting")]
    public float BaseHeight;   

    [Header("private setting")]
    bool IsSetObjectHeight = false;
    float ObjectHeight;

    void Update()
    {
        bool hasVrFocus = OVRPlugin.hasVrFocus;

        if (hasVrFocus)
        {
            if (!IsSetObjectHeight)
            {
                OVRCameraRig OCR = OVRManager.instance.GetComponentInChildren<OVRCameraRig>();

                if (OCR.isActiveAndEnabled)
                {
                    Transform centerEyeAnchor = OCR.centerEyeAnchor; //取得用戶的眼部高度
                    //Debug.LogWarning(centerEyeAnchor.position); //顯示幕前眼部高度

                    //計算物件合理高度
                    ObjectHeight = centerEyeAnchor.position.y - BaseHeight;

                    AdaptiveHeightContainer.transform.DOLocalMoveY(ObjectHeight, 0f);
                }

                IsSetObjectHeight = true;
            }
        }

        
        //OVRHand
    }
}
