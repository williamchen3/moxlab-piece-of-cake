using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using Random = System.Random;
using UnityEngine.SceneManagement;
using Oculus.Interaction;

public class GameController : MonoBehaviour
{
    [Header("Well")]
    public GameObject WAII_B;

    #region Flag List 
    [Header("List")]
    List<int> ColorList = new List<int>() { 0, 1, 2, 3, 4 };
    List<int> OrderList = new List<int>() { 1, 2, 3, 4, 5 };
    #endregion

    #region Const Value
    [Header("Const Value")]

    public const string RedColorValue = "#AE2012";
    public const string TiffanyColorValue = "#94D2BD";
    public const string IvoryColorValue = "#E9D8A6";
    public const string BlueColorValue = "#005F73";
    public const string OrangeColorValue = "#EE9B00";

    //public const string GreenDarkColorValue = "#7C6A0A";
    //public const string GreenLightColorValue = "#BABD8D";
    //public const string PinkColorValue = "#FFDAC6";
    //public const string YellowColorValue = "#FA9500";
    //public const string OrangeColorValue = "#EB6424";
    #endregion

    #region Material Ball
    [Header("Material Ball")]
    public Material MaterialBall_1;
    public Material MaterialBall_2;
    public Material MaterialBall_3;
    public Material MaterialBall_4;    
    public Material MaterialBall_5;
    #endregion

    #region Timer
    [Header("Timer")]
    //public TextMeshPro UI_Timer;
    public TextMeshPro UI_Timer_ss;
    public TextMeshPro UI_Timer_ms;

    public TextMeshPro UI_Timer_ss_small;
    public TextMeshPro UI_Timer_ms_small;    

    public GameObject Timer_ss;
    public GameObject Timer_dot;
    public GameObject Timer_ms;

    public GameObject Timer_ss_small;
    public GameObject Timer_dot_small;
    public GameObject Timer_ms_small;

    public TextMeshPro UI_Timer_ScoreBoard;
    public TextMeshPro UI_Timer_BestScoreBoard;
    #endregion

    [Header("gameobject - Easter egg item logo")]
    public GameObject MainLogo;
    public GameObject ProducerLogo;

    [Header("gameobject")]
    public GameObject BellButton;

    [Header("gameobject - grab")]
    public GameObject GrabCones;
    public GameObject GrabSphereNormal;
    public GameObject GrabSphereLarge;
    public GameObject GrabRectangle;    
    public GameObject GrabCylinder;

    [Header("GrabObjectRegister - grab")]
    public GrabObjectRegister GrabCones_GrabObjectRegister;
    public GrabObjectRegister GrabSphereNormal_GrabObjectRegister;
    public GrabObjectRegister GrabSphereLarge_GrabObjectRegister;
    public GrabObjectRegister GrabRectangle_GrabObjectRegister;
    public GrabObjectRegister GrabCylinder_GrabObjectRegister;

    //[Header("Container Gather")]
    //public GameObject ContainerGather_0;
    //public GameObject ContainerGather_1;
    //public GameObject ContainerGather_2;
    //public GameObject ContainerGather_3;
    //public GameObject ContainerGather_4;

    [Header("ContainerObjectRegister - Container")]
    public ContainerObjectRegister ContainerPenHolderOne_ContainerObjectRegister;
    public ContainerObjectRegister ContainerPenHolderTwo_ContainerObjectRegister;
    public ContainerObjectRegister ContainerBowl_ContainerObjectRegister;
    public ContainerObjectRegister ContainerCup_ContainerObjectRegister;
    public ContainerObjectRegister ContainerPaperCup_ContainerObjectRegister;

    [Header("gameobject - Container")]
    public GameObject ContainerPenHolderOne;
    public GameObject ContainerPenHolderTwo;
    public GameObject ContainerBowl;
    public GameObject ContainerCup;
    public GameObject ContainerPaperCup;

    [Header("gameobject - TableStand")]
    public GameObject TableStand;    

    [Header("gameobject - CollisionLauncher")]
    public CollisionLauncher ContainerPenHolderOne_CollisionLauncher;
    public CollisionLauncher ContainerPenHolderTwo_CollisionLauncher;
    public CollisionLauncher ContainerBowl_CollisionLauncher;
    public CollisionLauncher ContainerCup_CollisionLauncher;
    public CollisionLauncher ContainerPaperCup_CollisionLauncher;

    [Header("gameobject - CollisionLauncher")]
    public BorderColliderLauncher BorderCollider_0_CollisionLauncher;
    public BorderColliderLauncher BorderCollider_1_CollisionLauncher;
    public BorderColliderLauncher BorderCollider_2_CollisionLauncher;
    public BorderColliderLauncher BorderCollider_3_CollisionLauncher;

    [Header("gameobject - EasterEggCollisionLauncher")]
    public EasterEggCollisionLauncher TableStand_EasterEggCollisionLauncher;

    [Header("variable")]
    bool IsGameStart = false;
    bool IsGameOver = false;
    int CurrentOrder = 1;
    bool IsTableStandEnable = false;

    [Header("UI Object")]
    public GameObject GameOver;
    public GameObject ReplayText;
    public GameObject ScoreBoard;
    public GameObject BestScoreBoard;
    public GameObject WellDone;
    public GameObject HowtoPlay;
    public GameObject StartGameTips;
    public GameObject StartGameTips_Arrow;

    [Header("initial position grab")]
    Vector3 InitialPosition_Cones;
    Vector3 InitialPosition_SphereNormal;
    Vector3 InitialPosition_SphereLarge;
    Vector3 InitialPosition_Rectangle;
    Vector3 InitialPosition_Cylinder;

    [Header("Audio")]
    public AudioTrigger GameFailAudioTrigger;
    public AudioTrigger GameSuccessAudioTrigger;

    #region Container position
    [Header("Container position")]
    Vector3[] InitialPosition_ContainerPenHolderOne = new Vector3[] {
    new Vector3(-0.15f, 0.96f, 0.94f),
    new Vector3(-0.726f, 0.958f, 0.578f),
    new Vector3(-0.154f, 0.958f, 0.518f),
    new Vector3(-0.545f, 0.958f, 0.939f),
    new Vector3(-0.45f, 0.96f, 0.398f),
    };

    Vector3[] InitialPosition_ContainerPenHolderTwo = new Vector3[] {
    new Vector3( 0.73f, 0.958f, 0.723f),
    new Vector3(0.564f, 0.977f, 0.768f),
    new Vector3(-0.669f, 0.956f, 0.811f),
    new Vector3(0.332f, 0.949f, 0.536f),
    new Vector3(0.57f, 0.958f, 0.924f),
    };

    Vector3[] InitialPosition_ContainerBowl = new Vector3[] {
    new Vector3(0.226f, 0.938f, 0.857f),
    new Vector3(-0.342f, 0.938f, 0.779f),
    new Vector3(0.226f, 0.938f, 0.604f),
    new Vector3(0.665f, 0.938f, 0.72f),
    new Vector3(0.226f, 0.938f, 0.857f),
    };

    Vector3[] InitialPosition_ContainerCup = new Vector3[] {
    new Vector3(0.153f, 0.930f, 0.532f),
    new Vector3(0.153f, 0.928f, 0.823f),
    new Vector3(-0.060f, 0.933f, 0.911f),
    new Vector3(0.017f, 0.958f, 0.917f),
    new Vector3(-0.009f, 0.926f, 0.725f),
    };

    Vector3[] InitialPosition_ContainerPaperCup = new Vector3[] {
    new Vector3(-0.538f, 1.25f, 0.722f),
    new Vector3(0.669f, 1.24f, 0.667f),
    new Vector3( 0.727f, 0.984f, 0.825f),
    new Vector3(-0.668f, 0.981f, 0.567f),
    new Vector3(-0.24f, 0.98f, 0.53f),
    };

    Vector3[] InitialPosition_TableStand = new Vector3[] {
    new Vector3(-0.685f, 1.102f, 0.7f),
    new Vector3(0.697f, 1.10f, 0.71f),
    Vector3.zero,
    new Vector3(-0.038f, 1.10f, 0.977f),
    new Vector3(-0.68f, 1.10f, 0.857f),
    };

    Vector3[] InitialRotation_TableStand = new Vector3[] {
    new Vector3(0, 155.0f, 0),
    new Vector3(0, -135.0f, 0),
    Vector3.zero,
    new Vector3(0,-179.0f,0),
    new Vector3(0, -0.406f, 0),
    };
    #endregion

    float[] InitialPositionX_GrabItem = new float[] {
    0.35f,
    0.15f,
    0,
    -0.15f,
    -0.35f,
    };

    #region Timer
    float timer_f = 0.0f;
    #endregion

    //void SetContainerGather()
    //{

    //    //尋找容器內的物件，並且設定到實體物件
    //    Random rand = new Random();

    //    GameObject mContainerGather = new GameObject();

    //    switch (rand.Next(0, 5))
    //    {
    //        case 0:
    //            mContainerGather = ContainerGather_0;
    //            break;
    //        case 1:
    //            mContainerGather = ContainerGather_1;
    //            break;
    //        case 2:
    //            mContainerGather = ContainerGather_2;
    //            break;
    //        case 3:
    //            mContainerGather = ContainerGather_3;
    //            break;
    //        case 4:
    //            mContainerGather = ContainerGather_4;
    //            break;
    //        default:
    //            mContainerGather = ContainerGather_0;
    //            break;
    //    }

    //    ContainerPenHolderOne = mContainerGather.transform.Find("PenHolderOne").gameObject;
    //    ContainerPenHolderTwo = mContainerGather.transform.Find("PenHolderTwo").gameObject;
    //    ContainerBowl = mContainerGather.transform.Find("Bowl").gameObject;
    //    ContainerCup = mContainerGather.transform.Find("Cup").gameObject;
    //    ContainerPaperCup = mContainerGather.transform.Find("PaperCup").gameObject;
    //    TableStand = mContainerGather.transform.Find("TableStand").gameObject;

    //    ContainerPenHolderOne_ContainerObjectRegister = ContainerPenHolderOne.GetComponent<ContainerObjectRegister>();
    //    ContainerPenHolderTwo_ContainerObjectRegister = ContainerPenHolderTwo.GetComponent<ContainerObjectRegister>();
    //    ContainerBowl_ContainerObjectRegister = ContainerBowl.GetComponent<ContainerObjectRegister>();
    //    ContainerCup_ContainerObjectRegister = ContainerCup.GetComponent<ContainerObjectRegister>();
    //    ContainerPaperCup_ContainerObjectRegister = ContainerPaperCup.GetComponent<ContainerObjectRegister>();

    //    ContainerPenHolderOne_CollisionLauncher = ContainerPenHolderOne.transform.Find("BoxCollider_PenHolderOne").GetComponent<CollisionLauncher>();
    //    ContainerPenHolderTwo_CollisionLauncher = ContainerPenHolderTwo.transform.Find("BoxCollider_PenHolderTwo").GetComponent<CollisionLauncher>();
    //    ContainerBowl_CollisionLauncher = ContainerPenHolderTwo.transform.Find("BoxCollider_Bowl").GetComponent<CollisionLauncher>();
    //    ContainerCup_CollisionLauncher = ContainerPenHolderTwo.transform.Find("BoxCollider_Cup").GetComponent<CollisionLauncher>();
    //    ContainerPaperCup_CollisionLauncher = ContainerPenHolderTwo.transform.Find("BoxCollider_PaperCup").GetComponent<CollisionLauncher>();
    //}

    // Start is called before the first frame update

    void SetGrabItemInitialPositionX()
    {

        //尋找容器內的物件，並且設定到實體物件
        Random rand = new Random();

        List<int> InitialPositionXSeedList = new List<int>() { 0, 1, 2, 3, 4 };

        InitialPositionXSeedList = InitialPositionXSeedList.OrderBy(_ => rand.Next()).ToList();

        GrabCones.transform.DOLocalMoveX(InitialPositionX_GrabItem[InitialPositionXSeedList[0]], 0);
        GrabSphereNormal.transform.DOLocalMoveX(InitialPositionX_GrabItem[InitialPositionXSeedList[1]], 0);
        GrabSphereLarge.transform.DOLocalMoveX(InitialPositionX_GrabItem[InitialPositionXSeedList[2]], 0);
        GrabRectangle.transform.DOLocalMoveX(InitialPositionX_GrabItem[InitialPositionXSeedList[3]], 0);
        GrabCylinder.transform.DOLocalMoveX(InitialPositionX_GrabItem[InitialPositionXSeedList[4]], 0);
    }

    void SetContainerInitialPosition()
    {

        //尋找容器內的物件，並且設定到實體物件
        Random rand = new Random();

        int mGroupID = rand.Next(0, 5);

        ContainerPenHolderOne.transform.localPosition = InitialPosition_ContainerPenHolderOne[mGroupID];
        ContainerPenHolderTwo.transform.localPosition = InitialPosition_ContainerPenHolderTwo[mGroupID];
        ContainerBowl.transform.localPosition = InitialPosition_ContainerBowl[mGroupID];
        ContainerCup.transform.localPosition = InitialPosition_ContainerCup[mGroupID];
        ContainerPaperCup.transform.localPosition = InitialPosition_ContainerPaperCup[mGroupID];

        if (InitialPosition_TableStand[mGroupID] != Vector3.zero)
        {
            IsTableStandEnable = true;
            TableStand.transform.localPosition = InitialPosition_TableStand[mGroupID];
            TableStand.transform.localRotation = Quaternion.Euler(InitialRotation_TableStand[mGroupID]);
        }
        else
            IsTableStandEnable = false;        
    }

    // Start is called before the first frame update

    void Start()
    {

        SetGrabItemInitialPositionX();

        WAII_B.SetActive(true);

        InitialPosition_Cones = GrabCones.transform.localPosition;
        InitialPosition_SphereNormal = GrabSphereNormal.transform.localPosition;
        InitialPosition_SphereLarge = GrabSphereLarge.transform.localPosition;
        InitialPosition_Rectangle = GrabRectangle.transform.localPosition;
        InitialPosition_Cylinder = GrabCylinder.transform.localPosition;        

        GrabCones.SetActive(false);
        GrabSphereNormal.SetActive(false);
        GrabSphereLarge.SetActive(false);
        GrabRectangle.SetActive(false);
        GrabCylinder.SetActive(false);

        //For Test Off
        //GrabCones.SetActive(true);
        ContainerPenHolderOne_CollisionLauncher.OnTriggered.AddListener(CheckOrder);
        ContainerPenHolderTwo_CollisionLauncher.OnTriggered.AddListener(CheckOrder);
        ContainerBowl_CollisionLauncher.OnTriggered.AddListener(CheckOrder);
        ContainerCup_CollisionLauncher.OnTriggered.AddListener(CheckOrder);
        ContainerPaperCup_CollisionLauncher.OnTriggered.AddListener(CheckOrder);

        BorderCollider_0_CollisionLauncher.OnBorderCollidered.AddListener(SetGrabObjectPosition);
        BorderCollider_1_CollisionLauncher.OnBorderCollidered.AddListener(SetGrabObjectPosition);
        BorderCollider_2_CollisionLauncher.OnBorderCollidered.AddListener(SetGrabObjectPosition);
        BorderCollider_3_CollisionLauncher.OnBorderCollidered.AddListener(SetGrabObjectPosition);

        TableStand_EasterEggCollisionLauncher.OnEasterEggTriggered.AddListener(EasterEggTriggered);

        SetContainerInitialPosition();

        StartGameTipsArrowAnim();
    }

    private void EasterEggTriggered()
    {

        MainLogo.SetActive(false);
        ProducerLogo.SetActive(true);
        GameSuccessAudioTrigger.PlayAudio();
        //throw new NotImplementedException();
    }

    private void SetGrabObjectPosition(GrabTypes mGrabType) {

        switch (mGrabType)
        {
            case GrabTypes.Cones:
                GrabCones.transform.localPosition = InitialPosition_Cones;
                break;
            case GrabTypes.SphereNormal:
                GrabSphereNormal.transform.localPosition = InitialPosition_SphereNormal;
                break;
            case GrabTypes.SphereLarge:
                GrabSphereLarge.transform.localPosition = InitialPosition_SphereLarge;
                break;
            case GrabTypes.Rectangle:
                GrabRectangle.transform.localPosition = InitialPosition_Rectangle;
                break;
            case GrabTypes.Cylinder:
                GrabCylinder.transform.localPosition = InitialPosition_Cylinder;
                break;
        }
    }

    private void CheckOrder(int mOrder, bool mIsMetchColor, GameObject mContainerType)
    {
        Debug.Log("mOrder: " + mOrder);
        Debug.Log("mIsMetchColor: " + mIsMetchColor);

        if (!mIsMetchColor)
            SetGameOver();

        if (mOrder == CurrentOrder)
        {
            if (mOrder == 5)
            {
                SetGameFinished();
            }
            else
            {
                CurrentOrder++;
                mContainerType.transform.GetComponent<AudioTrigger>().PlayAudio();
            }
        }
        else
        {
            SetGameOver();
        }

        
    }

    void SetGameFinished()
    {
        GameSuccessAudioTrigger.PlayAudio();

        SetScoreBoardText();

        WellDone.SetActive(true);
        ScoreBoard.SetActive(true);        
        ReplayText.SetActive(true);

        IsGameStart = false;

        BellButton.SetActive(true);

        IsGameOver = true;

        UpdateBestScore();
    }

    void SetGameOver() {
        GameFailAudioTrigger.PlayAudio();

        GameOver.SetActive(true);
        ReplayText.SetActive(true);
        

        IsGameStart = false;

        BellButton.SetActive(true);

        IsGameOver = true;
    }

    private void UpdateBestScore()
    {
        BestScoreBoard.SetActive(true);

        //throw new NotImplementedException();

        float BestScore = PlayerPrefs.GetFloat("BestScore", 999.99f);

        if (timer_f < BestScore)
        {
            PlayerPrefs.SetFloat("BestScore", timer_f);
            BestScore = timer_f;
        }


        TimeSpan mTime = TimeSpan.FromSeconds(BestScore);

        string Seconds_Str;
        string Milliseconds_Str;
        int Milliseconds = mTime.Milliseconds / 10;

        if (mTime.TotalSeconds < 10)
            Seconds_Str = "0" + (int)mTime.TotalSeconds;
        else
            Seconds_Str = ((int)mTime.TotalSeconds).ToString();

        if (Milliseconds < 10)
            Milliseconds_Str = "0" + Milliseconds;
        else
            Milliseconds_Str = Milliseconds.ToString();

        UI_Timer_BestScoreBoard.text = Seconds_Str + ":" + Milliseconds_Str;
    }

    void DisableUI() {

        WellDone.SetActive(false);
        ScoreBoard.SetActive(false);
        GameOver.SetActive(false);
        ReplayText.SetActive(false);
        BestScoreBoard.SetActive(false);
    }

    Material GetMaterialBall(int mOrder) {

        Material mMaterial;

        switch (mOrder)
        {
            case 1:
                mMaterial = MaterialBall_1;
                break;
            case 2:
                mMaterial = MaterialBall_2;
                break;
            case 3:
                mMaterial = MaterialBall_3;
                break;
            case 4:
                mMaterial = MaterialBall_4;
                break;
            case 5:
                mMaterial = MaterialBall_5;
                break;
            default:
                mMaterial = MaterialBall_1;
                break;
        }

        return mMaterial;
    }

    #region Animation

    void StartGameTipsArrowAnim() {
        StartGameTips_Arrow.transform.DOLocalMoveY(-0.145f, 0.75f).From(-0.12f).SetEase(Ease.Linear).OnComplete(() =>
        {
            StartGameTips_Arrow.transform.DOLocalMoveY(-0.12f, 0.75f).From(-0.145f).SetEase(Ease.Linear).OnComplete(() =>
            {
                StartGameTipsArrowAnim();
            });
        });
    }

    #endregion

    public void GameStart()
    {
        if (IsGameOver)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        //For Test Off
        //Random rand = new Random();

        //ColorList = ColorList.OrderBy(_ => rand.Next()).ToList();
        //OrderList = OrderList.OrderBy(_ => rand.Next()).ToList();

        //GrabCones_GrabObjectRegister.ColorType = (ColorTypes)ColorList[0];
        //GrabCones_GrabObjectRegister.MaterialBall = GetMaterialBall(OrderList[0]);
        //GrabCones_GrabObjectRegister.Order = OrderList[0];
        //GrabCones_GrabObjectRegister.Init();
        //return;
        //For Test Off


        #region Ranmdom Grab Item

        Random rand = new Random();

        ColorList = ColorList.OrderBy(_ => rand.Next()).ToList();
        OrderList = OrderList.OrderBy(_ => rand.Next()).ToList();

        GrabCones_GrabObjectRegister.ColorType = (ColorTypes)ColorList[0];
        GrabCones_GrabObjectRegister.MaterialBall = GetMaterialBall(OrderList[0]);
        GrabCones_GrabObjectRegister.Order = OrderList[0];
        GrabCones_GrabObjectRegister.GrabType = GrabTypes.Cones;
        GrabCones_GrabObjectRegister.Init();

        GrabSphereNormal_GrabObjectRegister.ColorType = (ColorTypes)ColorList[1];
        GrabSphereNormal_GrabObjectRegister.MaterialBall = GetMaterialBall(OrderList[1]);
        GrabSphereNormal_GrabObjectRegister.Order = OrderList[1];
        GrabSphereNormal_GrabObjectRegister.GrabType = GrabTypes.SphereNormal;
        GrabSphereNormal_GrabObjectRegister.Init();

        GrabSphereLarge_GrabObjectRegister.ColorType = (ColorTypes)ColorList[2];
        GrabSphereLarge_GrabObjectRegister.MaterialBall = GetMaterialBall(OrderList[2]);
        GrabSphereLarge_GrabObjectRegister.Order = OrderList[2];
        GrabSphereLarge_GrabObjectRegister.GrabType = GrabTypes.SphereLarge;
        GrabSphereLarge_GrabObjectRegister.Init();

        GrabRectangle_GrabObjectRegister.ColorType = (ColorTypes)ColorList[3];
        GrabRectangle_GrabObjectRegister.MaterialBall = GetMaterialBall(OrderList[3]);
        GrabRectangle_GrabObjectRegister.Order = OrderList[3];
        GrabRectangle_GrabObjectRegister.GrabType = GrabTypes.Rectangle;
        GrabRectangle_GrabObjectRegister.Init();

        GrabCylinder_GrabObjectRegister.ColorType = (ColorTypes)ColorList[4];
        GrabCylinder_GrabObjectRegister.MaterialBall = GetMaterialBall(OrderList[4]);
        GrabCylinder_GrabObjectRegister.Order = OrderList[4];
        GrabCylinder_GrabObjectRegister.GrabType = GrabTypes.Cylinder;
        GrabCylinder_GrabObjectRegister.Init();
        #endregion

        #region Ranmdom Container Item       

        ColorList = ColorList.OrderBy(_ => rand.Next()).ToList();

        ContainerPenHolderOne_ContainerObjectRegister.ColorType = (ColorTypes)ColorList[0];
        ContainerPenHolderOne_ContainerObjectRegister.ContainerType = ContainerTypes.PenHolderOne;
        ContainerPenHolderOne_ContainerObjectRegister.Init();

        ContainerPenHolderTwo_ContainerObjectRegister.ColorType = (ColorTypes)ColorList[1];
        ContainerPenHolderOne_ContainerObjectRegister.ContainerType = ContainerTypes.PenHolderTwo;
        ContainerPenHolderTwo_ContainerObjectRegister.Init();

        ContainerBowl_ContainerObjectRegister.ColorType = (ColorTypes)ColorList[2];
        ContainerBowl_ContainerObjectRegister.ContainerType = ContainerTypes.Bowl;
        ContainerBowl_ContainerObjectRegister.Init();

        ContainerCup_ContainerObjectRegister.ColorType = (ColorTypes)ColorList[3];
        ContainerCup_ContainerObjectRegister.ContainerType = ContainerTypes.Cup;
        ContainerCup_ContainerObjectRegister.Init();

        ContainerPaperCup_ContainerObjectRegister.ColorType = (ColorTypes)ColorList[4];
        ContainerPaperCup_ContainerObjectRegister.ContainerType = ContainerTypes.PaperCup;
        ContainerPaperCup_ContainerObjectRegister.Init();

        #endregion

        this.transform.DOScale(1, 0.35f).OnComplete(() =>
        {
            StartGameTips_Arrow.transform.DOPause();

            BellButton.SetActive(false);
            HowtoPlay.SetActive(false);
            StartGameTips.SetActive(false);

            DisableUI();

            if (IsTableStandEnable)
                TableStand.SetActive(true);
            else
                TableStand.SetActive(false);

            //TableStand_Board.SetActive(true);
        });

        this.transform.DOScale(1, 0.55f).OnComplete(() =>
        {
            ContainerPenHolderOne.SetActive(true);
            ContainerPenHolderTwo.SetActive(true);
            ContainerBowl.SetActive(true);
            ContainerCup.SetActive(true);
            ContainerPaperCup.SetActive(true);
        });

        this.transform.DOScale(1, 0.95f).OnComplete(() =>
        {
            GrabCones.SetActive(true);
            GrabSphereNormal.SetActive(true);
            GrabSphereLarge.SetActive(true);
            GrabRectangle.SetActive(true);
            GrabCylinder.SetActive(true);

            IsGameStart = true;
        });
    }

    float myTimer;

    // Update is called once per frame
    void Update()
    {
        if (!IsGameStart)
            return;

        #region TrackGrabItemPositionY
        myTimer += Time.deltaTime;

        if (myTimer >= 3)
        {
            TrackGrabItemPositionY();
            myTimer = 0;
        }
        #endregion

        #region Timer
        timer_f += Time.deltaTime;
        TimeSpan time = TimeSpan.FromSeconds(timer_f);

        string Seconds_Str;
        string Milliseconds_Str;
        int Milliseconds = time.Milliseconds / 10;

        if (time.TotalSeconds < 10)
            Seconds_Str = "0" + (int)time.TotalSeconds;
        else
            Seconds_Str = ((int)time.TotalSeconds).ToString();

        if (Milliseconds < 10)
            Milliseconds_Str = "0" + Milliseconds;
        else
            Milliseconds_Str = Milliseconds.ToString();

        //Debug.Log(Seconds + ":" + Milliseconds);

        //UI_Timer.text = Seconds_Str + ":" + Milliseconds_Str;

        if ((int)time.TotalSeconds <= 99)
        {
            UI_Timer_ss.text = Seconds_Str;
            UI_Timer_ms.text = Milliseconds_Str.ToString();

            Timer_ss.SetActive(true);
            Timer_dot.SetActive(true);
            Timer_ms.SetActive(true);
            Timer_ss_small.SetActive(false);
            Timer_dot_small.SetActive(false);
            Timer_ms_small.SetActive(false);
        }
        else
        {
            UI_Timer_ss_small.text = Seconds_Str;
            UI_Timer_ms_small.text = Milliseconds_Str.ToString();


            Timer_ss.SetActive(false);
            Timer_dot.SetActive(false);
            Timer_ms.SetActive(false);
            Timer_ss_small.SetActive(true);
            Timer_dot_small.SetActive(true);
            Timer_ms_small.SetActive(true);
        }

        //UI_Timer_ss.text = Seconds_Str;
        //UI_Timer_ms.text = Milliseconds_Str.ToString();
        #endregion
    }

    void TrackGrabItemPositionY() {

        //解決物件會突然消失的問題(墜落到不知道哪裡的地方)

        if (GrabCones.transform.localPosition.y <= -10.0f)        
            GrabCones.transform.localPosition = InitialPosition_Cones;

        if (GrabSphereNormal.transform.localPosition.y <= -10.0f)
            GrabSphereNormal.transform.localPosition = InitialPosition_SphereNormal;

        if (GrabSphereLarge.transform.localPosition.y <= -10.0f)
            GrabSphereLarge.transform.localPosition = InitialPosition_SphereLarge;

        if (GrabRectangle.transform.localPosition.y <= -10.0f)
            GrabRectangle.transform.localPosition = InitialPosition_Rectangle;

        if (GrabCylinder.transform.localPosition.y <= -10.0f)
            GrabCylinder.transform.localPosition = InitialPosition_Cylinder;


    }

    void SetScoreBoardText()
    {
        #region Timer
        TimeSpan mTime = TimeSpan.FromSeconds(timer_f);

        string Seconds_Str;
        string Milliseconds_Str;
        int Milliseconds = mTime.Milliseconds / 10;

        if (mTime.TotalSeconds < 10)
            Seconds_Str = "0" + (int)mTime.TotalSeconds;
        else
            Seconds_Str = ((int)mTime.TotalSeconds).ToString();

        if (Milliseconds < 10)
            Milliseconds_Str = "0" + Milliseconds;
        else
            Milliseconds_Str = Milliseconds.ToString();        

        UI_Timer_ScoreBoard.text = Seconds_Str + ":" + Milliseconds_Str;
        #endregion
    }
}
