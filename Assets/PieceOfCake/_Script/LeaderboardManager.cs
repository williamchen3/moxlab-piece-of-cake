using Oculus.Platform;
using Oculus.Platform.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

//LeaderboardManager: 功能說明
//排行榜功能，與 Quest SDK 整合的部分

public class LeaderboardManager : MonoBehaviour
{
    // API NAME for the leaderboard where we store how many matches the user has won
    private const string HIGHEST_SCORE = "HIGHEST_SCORE";

	// the top number of entries to query
	private const int TOP_N_COUNT = 30;

	// Query all data or just only TOP_N_COUNT number 
	private const bool IS_NEED_QUERYALL = false;

	// how often to poll the service for leaderboard updates 
	private const float LEADERBOARD_POLL_FREQ = 30.0f;

	// the next time to check for leaderboard updates 
	private float m_nextCheckTime;

	//// whether we've found the local user's entry yet
	//private bool m_foundLocalUserHighScore;

	// cache to hold high-score leaderboard entries as they come in
	private volatile SortedDictionary<int, LeaderboardEntry> m_highScoresForAll;
	private volatile SortedDictionary<int, LeaderboardEntry> m_highScoresForMe;

	// callback to deliver the high-scores leaderboard entries (All)
	private OnHighScoreForAllUpdated m_highScoreForAllCallback;	
	private OnHighScoreForMeUpdated m_highScoreForMeCallback;


	public static LeaderboardManager instance = null;

	void Awake()
	{
		Core.Initialize();

		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);

		DontDestroyOnLoad(gameObject);
	}

	//No Use
	public void CheckForUpdates()
	{
		if (Time.time >= m_nextCheckTime)
		{
			m_nextCheckTime = Time.time + LEADERBOARD_POLL_FREQ;

			//QueryMostWinsLeaderboard();
			//QueryHighScoreLeaderboard();
		}
	}


	#region Submit Scores

	public event Action OnSubmitScoresComplete;

	public void SubmitScores(uint mScore)
	{
		if (mScore > 0)
			Leaderboards.WriteEntry(HIGHEST_SCORE, mScore).OnComplete(SubmitScoresComplete);
		else
		{
			//直接呼叫OnSubmitScoresComplete.Invoke();會錯誤，因此採用Dotween處理
			this.transform.DOScaleZ(0, 0.10f).OnComplete(() =>
			{
				OnSubmitScoresComplete.Invoke();
			});
		}	
    }

	private void SubmitScoresComplete(Message<bool> message)
	{
		if (message.Data) //主要判斷分數是否有更新，超過排行榜目前的分數
		{
			//do sth.		
		}
		else
		{
			//do sth.		
		}

		OnSubmitScoresComplete.Invoke();
	}
	#endregion


	#region Highest Score Board Update For All

	public delegate void OnHighScoreForAllUpdated(SortedDictionary<int, LeaderboardEntry> entries);

	public OnHighScoreForAllUpdated HighScoreForAllUpdatedCallback
	{
		set { m_highScoreForAllCallback = value; }
	}
	#endregion

	#region Highest Score Board For All

	public void QueryHighScoreForAll(LeaderboardFilterType mLeaderboardFilterType)
	{
		// if a query is already in progress, don't start a new one.
		if (m_highScoresForAll != null)
			return;

		m_highScoresForAll = new SortedDictionary<int, LeaderboardEntry>();		

		Leaderboards.GetEntries(HIGHEST_SCORE, TOP_N_COUNT, mLeaderboardFilterType,
			LeaderboardStartAt.Top).OnComplete(HighestScoreGetEntriesCallback);


	}

	void HighestScoreGetEntriesCallback(Message<LeaderboardEntryList> msg)
	{
		if (!msg.IsError)
		{
			foreach (LeaderboardEntry entry in msg.Data)
			{
				m_highScoresForAll[entry.Rank] = entry;
			}

			// results might be paged for large requests
			if (msg.Data.HasNextPage && IS_NEED_QUERYALL)
			{
				Leaderboards.GetNextEntries(msg.Data).OnComplete(HighestScoreGetEntriesCallback);
				return;
			}
		}
		// else an error is returned if the local player isn't ranked - we can ignore that

		if (m_highScoreForAllCallback != null)
		{
			m_highScoreForAllCallback(m_highScoresForAll);
		}

		m_highScoresForAll = null;
	}

	#endregion


	#region Highest Score Board Update For Me

	public delegate void OnHighScoreForMeUpdated(SortedDictionary<int, LeaderboardEntry> entries);

	public OnHighScoreForMeUpdated HighScoreForMeUpdatedCallback
	{
		set { m_highScoreForMeCallback = value; }
	}
	#endregion

	#region Highest Score Board For Me

	public void QueryHighScoreForMe(LeaderboardFilterType mLeaderboardFilterType)
	{
		if (m_highScoresForMe != null)
			return;

		m_highScoresForMe = new SortedDictionary<int, LeaderboardEntry>();

		Leaderboards.GetEntries(HIGHEST_SCORE, 1, mLeaderboardFilterType,
			LeaderboardStartAt.CenteredOnViewer).OnComplete(HighestScoreForMeGetEntriesCallback);
	}

	void HighestScoreForMeGetEntriesCallback(Message<LeaderboardEntryList> msg)
	{
		if (!msg.IsError)
		{
			foreach (LeaderboardEntry entry in msg.Data)
			{
				m_highScoresForMe[entry.Rank] = entry;
			}
		}
		// else an error is returned if the local player isn't ranked - we can ignore that

		if (m_highScoreForMeCallback != null)
		{
			m_highScoreForMeCallback(m_highScoresForMe);
		}
		m_highScoresForMe = null;
	}

	#endregion


	#region For Test 
	public void TestSumbitScores()
	{
		//instance.SubmitScores(8);
		//QueryHighScoreLeaderboardByMyPosition();
		//QueryHighScoreForAll();
	}

	#endregion


	#region How to use Update delegate event (Simple code):    

	//void Start()
	//{
	//	LeaderboardManager.instance.HighScoreForAllUpdatedCallback = HighestScoreForAllCallback;
	//	LeaderboardManager.instance.HighScoreForMeUpdatedCallback = HighestScoreForMeCallback;
	//}

	//void HighestScoreForAllCallback(SortedDictionary<int, LeaderboardEntry> entries)
	//{
	//	//foreach (Transform entry in m_highestScoresLeaderboard.transform)
	//	//{
	//	//	Destroy(entry.gameObject);
	//	//}
	//	//foreach (var entry in entries.Values)
	//	//{
	//	//	GameObject label = Instantiate(m_leaderboardEntryPrefab);
	//	//	label.transform.SetParent(m_highestScoresLeaderboard.transform, false);
	//	//	label.GetComponent<Text>().text =
	//	//		string.Format("{0} - {1} - {2}", entry.Rank, entry.User.OculusID, entry.Score);
	//	//}

	//	Debug.Log(entries);
	//}

	#endregion
}
