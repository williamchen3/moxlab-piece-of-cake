using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnTriggeredEvent : UnityEvent<int,bool, GameObject> { }

public class CollisionLauncher : MonoBehaviour
{
    bool IsLauncher = false;

    public OnTriggeredEvent OnTriggered = new OnTriggeredEvent();
    //public event Action OnTriggered = delegate { };

    private void OnTriggerEnter(Collider other)
    {

        if (IsLauncher)
            return;        

        if (other.tag != "GrabObject")
        {
            Debug.Log("No Grab Object");
            return;
        }

        IsLauncher = true;

        
        int mOrder = other.gameObject.GetComponent<GrabObjectRegister>().Order;
        bool mIsMetchColor;

        ColorTypes GrabObjectColor = other.gameObject.GetComponent<GrabObjectRegister>().ColorType;
        ColorTypes ContainerObjectColor = this.gameObject.GetComponentInParent<ContainerObjectRegister>().ColorType;

        //ContainerTypes ContainerObjectType = this.gameObject.GetComponentInParent<ContainerObjectRegister>().ContainerType;

        GameObject mGameObject = this.gameObject.transform.parent.Find("Audio").gameObject;



        mIsMetchColor = Enum.Equals(GrabObjectColor, ContainerObjectColor);

        OnTriggered.Invoke(mOrder, mIsMetchColor, mGameObject);
    }
}
