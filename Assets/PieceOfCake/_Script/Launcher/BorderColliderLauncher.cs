using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnBorderCollideredEvent : UnityEvent<GrabTypes> { }

public class BorderColliderLauncher : MonoBehaviour
{
    public OnBorderCollideredEvent OnBorderCollidered = new OnBorderCollideredEvent();

    private void OnTriggerEnter(Collider other)
    {
        GrabTypes GrabType = GrabTypes.Cones;        

        if (other.tag == "GrabObject")
        {
            GrabType = other.gameObject.GetComponent<GrabObjectRegister>().GrabType;
            OnBorderCollidered.Invoke(GrabType);           

        }
    }
}
