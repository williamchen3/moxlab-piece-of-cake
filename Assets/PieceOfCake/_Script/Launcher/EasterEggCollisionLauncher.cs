using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EasterEggCollisionLauncher : MonoBehaviour
{
    bool IsLauncher = false;

    public UnityEvent OnEasterEggTriggered = new UnityEvent();
    //public event Action OnTriggered = delegate { };

    private void OnCollisionEnter(Collision other)
    {
        if (IsLauncher)
            return;

        if (other.gameObject.tag != "GrabObject")
        {
            Debug.Log("No Grab Object");
            return;
        }

        IsLauncher = true;

        if (other.gameObject.name == "KinematicRectangle")
            OnEasterEggTriggered.Invoke();
    }

    private void OnCollisionStay(Collision other)
    {

    }

    private void OnTriggerEnter(Collider other)
    {
 
    }
}
