using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIAnimationController : MonoBehaviour
{
    [Header("Canvas UI")]
    public GameObject CanvasUI_Leaderboard;

    [Header("UI Title")]
    public GameObject UI_Title_The;
    public GameObject UI_Title_Red;
    public GameObject UI_Title_Button;
    public GameObject UI_Title_DTI;

    [Header("UI Score")]
    public GameObject Quad_Crown;
    public GameObject UI_BestScore;
    public GameObject UIText_CurrentScoreText;
    public GameObject UIText_CurrentScore;
    public GameObject UIText_GameOver;

    [Header("UI Button")]
    public GameObject Btn_Play;
    public GameObject Btn_Replay;
    public GameObject Btn_Leaderboard_Friend;
    public GameObject Btn_Leaderboard_Global;

    [Header("Game Button")]
    public GameObject GameButton;

    // Start is called before the first frame update

    private void Start()
    {
        //GameSatrtAnimation(); //For test
        //GameButtonIntoAnimation(); //For test
        //return;//For test

        IntoAnimation();        
        GameButton.transform.DOScaleY(0.0f, 0.0f); //坑：無法直接在編輯器設定Scale數值為0.0f否則縮放後會出錯，必須要用代碼設定
    }

    public void IntoAnimation_archive()
    {
        //Join : 和上一個動畫同步
        //Append : 等待前面動畫播完再撥放

        Sequence moveSequence = DOTween.Sequence();

        Tween Tween_0 = UI_Title_The.transform.DOLocalMoveZ(0, 0.55f).From(0.35f).SetEase(Ease.InSine);
        Tween Tween_1 = UI_Title_The.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_2 = UI_Title_Button.transform.DOLocalMoveZ(0, 0.55f).From(0.35f).SetDelay(0.15f).SetEase(Ease.OutSine);
        Tween Tween_3 = UI_Title_Button.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_4 = UI_Title_Red.transform.DOLocalMoveZ(-0.01f, 0.55f).From(0.35f).SetEase(Ease.InSine);
        Tween Tween_5 = UI_Title_Red.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_6 = UI_Title_DTI.transform.DOLocalMoveZ(-0.01f, 0.55f).From(0.35f).SetDelay(0.15f).SetEase(Ease.OutSine);
        Tween Tween_7 = UI_Title_DTI.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_8 = Btn_Play.transform.DOLocalMoveZ(0.25f, 0.35f).From(0.55f).SetEase(Ease.OutExpo);
        Tween Tween_9 = Btn_Play.transform.DOScaleX(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo);
        Tween Tween_10 = Btn_Play.transform.DOScaleY(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo);

        moveSequence.Append(Tween_0);
        moveSequence.Join(Tween_1);

        moveSequence.Join(Tween_2);
        moveSequence.Join(Tween_3);

        moveSequence.Join(Tween_4);
        moveSequence.Join(Tween_5);

        moveSequence.Join(Tween_6);
        moveSequence.Join(Tween_7);

        moveSequence.Join(Tween_8);
        moveSequence.Join(Tween_9);
        moveSequence.Join(Tween_10);

        moveSequence.SetDelay(1.55f);
    }

    public void IntoAnimation()
    {
        //Join : 和上一個動畫同步
        //Append : 等待前面動畫播完再撥放

        Sequence moveSequence = DOTween.Sequence();

        Tween Tween_0 = UI_Title_The.transform.DOLocalMoveZ(0, 0.55f).From(0.35f).SetEase(Ease.OutExpo);
        Tween Tween_1 = UI_Title_The.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_2 = UI_Title_Button.transform.DOLocalMoveZ(0, 0.55f).From(0.35f).SetDelay(0.15f).SetEase(Ease.OutExpo);
        Tween Tween_3 = UI_Title_Button.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_4 = UI_Title_Red.transform.DOLocalMoveZ(-0.01f, 0.55f).From(0.35f).SetEase(Ease.OutExpo);
        Tween Tween_5 = UI_Title_Red.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_6 = UI_Title_DTI.transform.DOLocalMoveZ(-0.01f, 0.55f).From(0.35f).SetDelay(0.15f).SetEase(Ease.OutExpo);
        Tween Tween_7 = UI_Title_DTI.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_8 = Btn_Play.transform.DOLocalMoveZ(0.25f, 0.35f).From(0.55f).SetEase(Ease.OutExpo);
        Tween Tween_9 = Btn_Play.transform.DOScaleX(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo);
        Tween Tween_10 = Btn_Play.transform.DOScaleY(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo);

        Tween Tween_11 = UI_Title_Red.transform.DOScaleX(0.0065f, 0.05f).From(0.005f).OnComplete(() =>
        {
            UI_Title_Red.transform.DOScaleX(0.005f, 0.05f);
        });

        Tween Tween_12 = UI_Title_Red.transform.DOScaleY(0.0065f, 0.15f).From(0.005f).OnComplete(() =>
        {
            UI_Title_Red.transform.DOScaleY(0.005f, 0.15f);
        });

        moveSequence.Append(Tween_0);
        moveSequence.Join(Tween_1); 

        moveSequence.Join(Tween_2);
        moveSequence.Join(Tween_3);

        moveSequence.Join(Tween_4);
        moveSequence.Join(Tween_5);

        moveSequence.Join(Tween_6);
        moveSequence.Join(Tween_7);

        moveSequence.Join(Tween_8);
        moveSequence.Join(Tween_9);
        moveSequence.Join(Tween_10);

        moveSequence.Append(Tween_11);
        moveSequence.Append(Tween_12);

        moveSequence.SetDelay(0.75f);
    }

    public void GameButtonIntoAnimation_archive()
    {
        Sequence moveSequence = DOTween.Sequence();

        Tween Tween_0 = GameButton.transform.DOLocalMoveY(0.808f, 0.85f).From(0.7315f).SetEase(Ease.OutExpo);
        Tween Tween_1 = GameButton.transform.DOScaleY(0.075f, 0.85f).From(0f).SetEase(Ease.OutExpo);


        moveSequence.SetDelay(1.35f);
    }

    public void GameSatrtAnimation()
    {
        //Join : 和上一個動畫同步
        //Append : 等待前面動畫播完再撥放

        Sequence moveSequence = DOTween.Sequence();

        Tween Tween_0 = Quad_Crown.transform.DOLocalMoveZ(0, 0.55f).From(0.35f).SetEase(Ease.OutExpo);
        Tween Tween_1 = Quad_Crown.transform.GetComponent<MeshRenderer>().material.DOFade(1, 0.35f).From(0);

        Tween Tween_2 = UI_BestScore.transform.DOLocalMoveZ(0, 0.55f).From(0.35f).SetDelay(0.15f).SetEase(Ease.OutExpo);
        Tween Tween_3 = UI_BestScore.transform.GetComponent<MeshRenderer>().material.DOFade(1, 0.35f).From(0);

        Tween Tween_4 = UIText_CurrentScoreText.transform.DOLocalMoveZ(0f, 0.55f).From(0.35f).SetEase(Ease.OutExpo);
        Tween Tween_5 = UIText_CurrentScoreText.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_6 = UIText_CurrentScore.transform.DOLocalMoveZ(0f, 0.55f).From(0.35f).SetDelay(0.15f).SetEase(Ease.OutExpo);
        Tween Tween_7 = UIText_CurrentScore.transform.GetComponent<TextMeshPro>().DOFade(1, 0.35f).From(0);

        Tween Tween_8 = GameButton.transform.DOLocalMoveY(0.808f, 0.85f).From(0.7315f).SetEase(Ease.OutExpo);
        Tween Tween_9 = GameButton.transform.DOScaleY(0.075f, 0.85f).From(0f).SetEase(Ease.OutExpo);

        moveSequence.Append(Tween_0);
        moveSequence.Join(Tween_1);

        moveSequence.Join(Tween_2);
        moveSequence.Join(Tween_3);

        moveSequence.Join(Tween_4);
        moveSequence.Join(Tween_5);

        moveSequence.Join(Tween_6);
        moveSequence.Join(Tween_7);

        moveSequence.Append(Tween_8);
        moveSequence.Join(Tween_9);

        moveSequence.SetDelay(0.15f);
    }

    public void GameOverAnimation()
    {
        //Join : 和上一個動畫同步
        //Append : 等待前面動畫播完再撥放

        Sequence moveSequence = DOTween.Sequence();

        Tween Tween_0 = UIText_GameOver.transform.DOLocalMoveY(-0.13f, 0.55f).From(-0.15f).SetEase(Ease.OutExpo);

        Tween Tween_1 = GameButton.transform.DOLocalMoveY(0.7315f, 0.45f).From(0.808f).SetEase(Ease.OutExpo);
        Tween Tween_2 = GameButton.transform.DOScaleY(0f, 0.45f).From(0.075f).SetEase(Ease.OutExpo);
            
        Tween Tween_3 = Btn_Replay.transform.DOLocalMoveZ(0.25f, 0.35f).From(0.55f).SetEase(Ease.OutExpo);
        Tween Tween_4 = Btn_Replay.transform.DOScaleX(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo);
        Tween Tween_5 = Btn_Replay.transform.DOScaleY(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo);

        moveSequence.Append(Tween_0);

        moveSequence.Append(Tween_1);
        moveSequence.Join(Tween_2);

        moveSequence.Append(Tween_3);
        moveSequence.Join(Tween_4);
        moveSequence.Join(Tween_5);

        Sequence leaderboardmoveSequence = DOTween.Sequence();
        
        Tween Tween_lbms_0 = CanvasUI_Leaderboard.transform.DOLocalMoveZ(0f, 0.95f).From(0.50f).SetEase(Ease.OutExpo);
        Tween Tween_lbms_1 = CanvasUI_Leaderboard.GetComponent<CanvasGroup>().DOFade(1.0f, 0.95f).From(0.0f);

        Tween Tween_lbms_2 = Btn_Leaderboard_Global.transform.DOLocalMoveZ(0.01f, 0.75f).From(0.35f).SetEase(Ease.OutExpo).SetDelay(0.1f);
        Tween Tween_lbms_3 = Btn_Leaderboard_Global.transform.DOScaleX(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo).SetDelay(0.1f);
        Tween Tween_lbms_4 = Btn_Leaderboard_Global.transform.DOScaleY(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo).SetDelay(0.1f);

        Tween Tween_lbms_5 = Btn_Leaderboard_Friend.transform.DOLocalMoveZ(0.01f, 0.75f).From(0.35f).SetEase(Ease.OutExpo).SetDelay(0.15f);
        Tween Tween_lbms_6 = Btn_Leaderboard_Friend.transform.DOScaleX(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo).SetDelay(0.15f);
        Tween Tween_lbms_7 = Btn_Leaderboard_Friend.transform.DOScaleY(0.075f, 0.45f).From(0.0f).SetEase(Ease.OutExpo).SetDelay(0.15f);

        leaderboardmoveSequence.Append(Tween_lbms_0);
        leaderboardmoveSequence.Join(Tween_lbms_1);

        leaderboardmoveSequence.Join(Tween_lbms_2);
        leaderboardmoveSequence.Join(Tween_lbms_3);
        leaderboardmoveSequence.Join(Tween_lbms_4);

        leaderboardmoveSequence.Join(Tween_lbms_5);
        leaderboardmoveSequence.Join(Tween_lbms_6);
        leaderboardmoveSequence.Join(Tween_lbms_7);
    }
}
