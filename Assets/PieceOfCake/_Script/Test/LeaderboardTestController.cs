using Oculus.Platform.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardTestController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		LeaderboardManager.instance.HighScoreForAllUpdatedCallback = HighestScoreForAllCallback;
		LeaderboardManager.instance.HighScoreForMeUpdatedCallback = HighestScoreForMeCallback;
	}

	void HighestScoreForAllCallback(SortedDictionary<int, LeaderboardEntry> entries)
	{
		//foreach (Transform entry in m_highestScoresLeaderboard.transform)
		//{
		//	Destroy(entry.gameObject);
		//}
		//foreach (var entry in entries.Values)
		//{
		//	GameObject label = Instantiate(m_leaderboardEntryPrefab);
		//	label.transform.SetParent(m_highestScoresLeaderboard.transform, false);
		//	label.GetComponent<Text>().text =
		//		string.Format("{0} - {1} - {2}", entry.Rank, entry.User.OculusID, entry.Score);
		//}

		Debug.Log(entries);
	}

	void HighestScoreForMeCallback(SortedDictionary<int, LeaderboardEntry> entries)
	{
		Debug.Log(entries);
	}


	// Update is called once per frame
	void Update()
    {
        
    }
}
