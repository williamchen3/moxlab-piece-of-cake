using Oculus.Platform;
using Oculus.Platform.Models;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

//LeaderboardController: 功能說明
//排行榜功能，與UI整合的部分

public class LeaderboardController : MonoBehaviour
{
    
    [Header("Prefab")]
    public GameObject LeaderBoardUser;

    [Header("GameObject")]
    public GameObject UserContent;
    public GameObject Canvas_Loading;

    [Header("TextMeshProUGUI")]
    public TextMeshProUGUI YourRank;

    [Header("Setting")]
    public bool? IsHighScoreForAll = null;

    private void Start()
    {
        LeaderboardManager.instance.HighScoreForAllUpdatedCallback = HighestScoreForAllCallback;
        LeaderboardManager.instance.HighScoreForMeUpdatedCallback = HighestScoreForMeCallback;
    }

    // Start is called before the first frame update
    public void EnableLeaderboard_All()
    {
        if (IsHighScoreForAll != true)
        {
            UserContent.SetActive(false);
            Canvas_Loading.SetActive(true);

            LeaderboardManager.instance.QueryHighScoreForAll(LeaderboardFilterType.None);
            LeaderboardManager.instance.QueryHighScoreForMe(LeaderboardFilterType.None);
            IsHighScoreForAll = true;
        }
    }

    public void EnableLeaderboard_Friends()
    {
        if (IsHighScoreForAll != false)
        {
            UserContent.SetActive(false);
            Canvas_Loading.SetActive(true);

            LeaderboardManager.instance.QueryHighScoreForAll(LeaderboardFilterType.Friends);
            LeaderboardManager.instance.QueryHighScoreForMe(LeaderboardFilterType.Friends);
            IsHighScoreForAll = false;
        }
    }

    void HighestScoreForAllCallback(SortedDictionary<int, LeaderboardEntry> entries)
    {
        UserContent.SetActive(true);
        Canvas_Loading.SetActive(false);

        foreach (Transform entry in UserContent.transform)
        {
            Destroy(entry.gameObject);
        }

        foreach (var entry in entries.Values)
        {
            GameObject User = Instantiate(LeaderBoardUser);
            User.transform.SetParent(UserContent.transform, false);

            User.GetComponent<LeaderBoardUserReceiver>().Init(entry.Rank, entry.User.DisplayName, entry.Score);

            //label.GetComponent<Text>().text =
            //    string.Format("{0} - {1} - {2}", entry.Rank, entry.User.OculusID, entry.Score);
        }

        Debug.Log(entries);
    }

    void HighestScoreForMeCallback(SortedDictionary<int, LeaderboardEntry> entries)
    {
        UserContent.SetActive(true);
        Canvas_Loading.SetActive(false);

        if (entries.Count == 0) {
            YourRank.text = "Your rank: 999999";
            return;
        }
      
        foreach (var entry in entries.Values)
        {
            YourRank.text = "Your rank: " + entry.Rank;
        }

        Debug.Log(entries);
    }
}
