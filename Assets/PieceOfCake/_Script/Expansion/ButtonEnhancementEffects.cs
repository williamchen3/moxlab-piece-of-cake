using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Oculus.Interaction;
using UnityEngine.Events;

public class ButtonEnhancementEffects : MonoBehaviour
{
    /*
    * 點擊後，增加增強特效。(顏色和淡出，外框擴散) 
    * 需搭配 Quest SDK 中的 RoundedBoxProperties, MaterialPropertyBlockEditor 使用
    * 需搭配 Unity asset 的 DoTween 套件使用
    */

    [Header("Properties")]
    public RoundedBoxProperties MainProperties;
    public MaterialPropertyBlockEditor MainMaterialPropertyEditor;

    [Header("Setting")]
    [Tooltip("Enhance the length of the animation 0.1 and 0.55")]
    public float Duration;

    [Tooltip("Enhanced animation scaling 1.1 and 1.5")]
    public float Scale;

    [Tooltip("Start alpha value, fade out or fade in")]
    public float AlphaStart;

    [Tooltip("End alpha value, fade out or fade in")]
    public float AlphaEnd;

    [Tooltip("Animation ease type")]
    public Ease EaseType;

    [SerializeField]
    private UnityEvent _whenEffectComplete;

    [Header("private variable")]
    float OriginalScaleX;
    float OriginalScaleY;

    #region Properties

    public UnityEvent WhenEffectComplete => _whenEffectComplete;

    #endregion

    private void Start()
    {
        OriginalScaleX = this.gameObject.transform.localScale.x;
        OriginalScaleY = this.gameObject.transform.localScale.y;
    }

    public void BeginEffects()
    {

        #region fade out or fade in
        Color MainColor = MainProperties.Color;  

        DOTween.To(delegate (float value)
        {
            MainProperties.Color = new Color(MainProperties.Color.r, MainProperties.Color.g, MainProperties.Color.b, value);

            MaterialPropertyBlock block = MainMaterialPropertyEditor.MaterialPropertyBlock;
            block.SetColor(Shader.PropertyToID("_Color"), MainProperties.Color);

        }, AlphaStart, AlphaEnd, Duration).SetEase(EaseType).OnComplete(() =>
        {            

        });
        #endregion

        #region Scale


        float NewScaleX = this.gameObject.transform.localScale.x * Scale;
        float NewScaleY = this.gameObject.transform.localScale.y * Scale;

        this.gameObject.transform.DOScale(new Vector3(NewScaleX, NewScaleY), Duration).SetEase(EaseType).OnComplete(() =>
        {
            this.gameObject.transform.localScale = new Vector3(OriginalScaleX, OriginalScaleY);

            WhenEffectComplete.Invoke();
        });
        #endregion
    }
}
