//For Test On

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;
using Oculus.Interaction;

/// <summary>
/// ***坑***
/// 1. Btn_Play和 Btn_RePlay，如果以 Select 做判斷，因為按鈕瞬間消失的關係，手部會卡在按鈕上
/// 2. GameButton的Position的x,y,z無法設定為0，否則會有按鈕殘影跟按鈕無法正常按下的問題 (如果MaxDistance和EnterHoverDistance都是0，則會出現抖動的情況，有設定數值就不會)
/// 3. 如果按鈕要放大，請縮放整個物件，不要單純縮放Visuals或模型，會造成觸控區域錯誤的狀況
/// 4. 要互動的物件PokeInteractable，不要使用SetActive開啟或關閉，會造成HandPokeInteractorFist和HandPokeInteractorPalm的錯誤。
/// 
/// ***特別筆記***
/// 1. Pock的功能如果要增加手掌和拳頭都可以觸碰物件，請加入
/// HandPokeInteractorFist
/// HandPokeInteractorPalm
/// 可以從Pock的範例抓InteractionRigOVR - Basic過來參考
/// </summary>

public class GameController_TRB : MonoBehaviour
{
  

    [Header("UI Button")]
    public GameObject Btn_Play;
    public GameObject Btn_RePlay;

    public GameObject Btn_Leaderboard_Friend;
    public GameObject Btn_Leaderboard_Global;

    [Header("UI")]
    public GameObject UI_Title;
    public GameObject UI_Score;

    public TextMeshPro UIText_CurrentScore;
    public TextMeshPro UIText_BestScore;

    public GameObject UIText_GameOver;

    public TextMeshPro UIText_Version;

    public TextMeshPro UIText_DebugMsg;

    public GameObject CanvasUI_Leaderboard;

    [Header("Game Button")]
    public MeshRenderer GameButton_MeshRenderer;
    public GameObject GameButton;
    public GameObject UIText_DontPress;
    public GameObject UIText_TapMe;

    [Header("Best Score Item")]    
    public GameObject Obj_Quad_Crown;
    public GameObject Obj_UI_BestScore;
    public GameObject Obj_UIText_BestScore;

   [Header("Resource")]
    public Material Btn_Green;
    public Material Btn_Red;

    [Header("Controller")]
    public UIAnimationController UIAnimationController;
    public LeaderboardController LeaderboardController;


    [Header("Other")]
    float timer;
    float timer_Target;

    [Header("Setting")]
    int BtnColor = 0;    //0是紅色,1是綠色    
    int Score = 0;
    int BestScore = 0;



    bool IsTimerLock = false;
    bool IsScoreAdded = false;
    bool IsGameOver = false;




    private void Start()
    {

        //For Test
        //PlayerPrefs.DeleteAll();


        //For Test


        CanvasUI_Leaderboard.SetActive(false);

        UI_Title.SetActive(true);

        UI_Score.SetActive(false);

        UIText_GameOver.SetActive(false);

        Btn_Play.SetActive(true);
        Btn_RePlay.SetActive(false);

        Btn_Leaderboard_Friend.SetActive(false);
        Btn_Leaderboard_Global.SetActive(false);

        GameButton.SetActive(true);

        BestScore = PlayerPrefs.GetInt("BestScore", 0);

        UIText_Version.text = "v" + Application.version;

        //OVRManager.isInsightPassthroughEnabled = true;
    }

    #region Game
    public void SetNewGame() {

        timer_Target = Random.Range(0.35f, 1.2f);
        timer = 0;
        BtnColor = 0;
        IsTimerLock = false;
        Score = 0;

        UI_Title.SetActive(false);

        UI_Score.SetActive(true);
        UIText_GameOver.SetActive(false);

        Btn_Play.SetActive(false);
        Btn_RePlay.SetActive(false);

        Btn_Leaderboard_Friend.SetActive(false);
        Btn_Leaderboard_Global.SetActive(false);

        GameButton.SetActive(true);

        CanvasUI_Leaderboard.SetActive(false);

        GameButton.GetComponent<InteractableUnityEventWrapper>().enabled = true;

        SFXManager.instance.PlayClip(SFXType.Tap);

        IsGameOver = false;

        //UIAnimationController.GameButtonIntoAnimation();

        UIAnimationController.GameSatrtAnimation();
    }

    void SetGameOver()
    {
        if (IsGameOver)
            return;

        UI_Score.SetActive(true);
        UIText_GameOver.SetActive(true);

        Btn_RePlay.SetActive(true);

        CanvasUI_Leaderboard.SetActive(true);

        Btn_Leaderboard_Friend.SetActive(true);
        Btn_Leaderboard_Global.SetActive(true);

        GameButton.GetComponent<InteractableUnityEventWrapper>().enabled = false;

        PlayerPrefs.SetInt("BestScore", BestScore);

        SFXManager.instance.PlayClip(SFXType.Warning);

        IsGameOver = true;

        UIAnimationController.GameOverAnimation();

        LeaderboardManager.instance.SubmitScores((uint)Score);

        LeaderboardManager.instance.OnSubmitScoresComplete += () => {

            ////延遲0.5秒後更新，避免讀不到最新的分數
            //this.transform.DOScaleZ(0, 0.15f).OnComplete(() =>
            //{

            //});
            LeaderboardController.IsHighScoreForAll = null; //Reset flag
            LeaderboardController.EnableLeaderboard_All();
        };
    }
    #endregion


    private void AddScore()
    {
        if (IsScoreAdded)
            return;

        if (IsGameOver)
            return;

        Score++;

        if (Score > BestScore)
        {
            BestScore = Score;
            SFXManager.instance.PlayClip(SFXType.Result);

            HighScoreEffect();
        }
        else        
            SFXManager.instance.PlayClip(SFXType.Tap);

        IsScoreAdded = true;
    }

 

    // Update is called once per frame
    void Update()
    {
        UIText_CurrentScore.text = Score + "";
        UIText_BestScore.text = BestScore + "";

        if (IsTimerLock)
            return;

        if (timer < timer_Target)
        {
            timer += Time.deltaTime;
        }
        else
        {
            //timer_Target = Random.Range(0.35f, 2.45f);

            switch (Random.Range(0, 5))
            {
                case 0:
                    timer_Target = 0.15f;
                    break;
                case 1:
                    timer_Target = 0.35f;
                    break;
                case 2:
                    timer_Target = 0.75f;
                    break;
                case 3:
                    timer_Target = 1.75f;
                    break;
                case 4:
                    timer_Target = 2.05f;
                    break;                    
            }

            timer = 0;            

            if (BtnColor == 1)
            {
                GameButton_MeshRenderer.material = Btn_Red;
                BtnColor = 0;

                UIText_DontPress.SetActive(true);                
                UIText_TapMe.SetActive(false);
            }
            else
            {
                GameButton_MeshRenderer.material = Btn_Green;
                BtnColor = 1;

                UIText_DontPress.SetActive(false);
                UIText_TapMe.SetActive(true);
            }
        }

        #region To Check open or close the System UI
    
        
        //Debug.LogWarning("hasVrFocus:" + hasVrFocus);
        #endregion

    }

    #region GameBtn Click Event

    public void GameBtn_OnHover()
    {
        UIText_DebugMsg.text = "On Hover";

        IsTimerLock = true;
        
        //「觸碰到」紅色方塊遊戲結束的版本
        //if (BtnColor == 0) //紅色        
        //{
        //    UIText_DebugMsg.text = "Game Over!";
        //    SetGameOver();
        //}
    }
    public void GameBtn_OnSelect()
    {
        UIText_DebugMsg.text = "On Select";

        //AddScore(); //「觸碰到」紅色方塊遊戲結束的版本

        //「壓下」紅色方塊遊戲結束的版本
        if (BtnColor == 0) //紅色
        {
            UIText_DebugMsg.text = "Game Over!";
            SetGameOver();
        }
        else        
            AddScore();        
    }


    public void GameBtn_OnUnSelect()
    {
        UIText_DebugMsg.text = "On UnSelect";
    }
    public void GameBtn_OnUnHover()
    {
        UIText_DebugMsg.text = "On UnHover";

        IsTimerLock = false;

        IsScoreAdded = false;
    }

    #endregion

    //#region PlayBtn Click Event
    //public void PlayBtn_OnSelect()
    //{
    //    SetNewGame();
    //}

    //public void PlayBtn_OnUnSelect() 
    //{
    //    SetNewGame();
    //}
    //#endregion

    //#region RePlayBtn Click Event
    //public void RePlayBtn_OnUnSelect()
    //{
    //    SetNewGame();
    //}
    //#endregion

    #region For Test
    public void PlaySoundTest()
    {
        SFXManager.instance.PlayClip(SFXType.Warning);
    }
    #endregion


    #region Application Lifecycle (Not Working)
    private void OnApplicationPause(bool pause)
    {
        if (pause)        
            Debug.Log("open the System UI");
        else
            Debug.Log("close the System UI");
    }

    //private void OnApplicationFocus(bool pause)
    //{
    //    if (pause)
    //        Debug.Log("open the System UI");
    //    else
    //        Debug.Log("close the System UI");        
    //}
    #endregion


    #region Effects
    public void HighScoreEffect()
    {
        //這邊寫死，如果物件有縮放要留意
        Obj_UI_BestScore.transform.DOScale(0.035f * 1.15f, 0.05f).From(0.035f).OnComplete(() =>
        {
            Obj_UI_BestScore.transform.DOScale(0.035f, 0.05f);
        });

        Obj_UIText_BestScore.transform.DOPunchRotation(new Vector3(0, 0, 35.0f), 1.35f, 10, 0.15f).OnComplete(() =>
        {
            Obj_UIText_BestScore.transform.DOLocalRotate(new Vector3(0, 0, 0f), 0.15f);
        });


        Obj_Quad_Crown.transform.DOScale(0.035f * 1.15f, 0.05f).From(0.035f).OnComplete(() =>
        {
            Obj_Quad_Crown.transform.DOScale(0.035f, 0.05f);
        });

        //Obj_Quad_Crown.transform.DOPunchRotation(new Vector3(0, 0, 15.0f), 0.55f, 5, 0.10f);
    }
    #endregion


}
