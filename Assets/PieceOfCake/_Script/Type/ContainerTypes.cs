public enum ContainerTypes
{
    PenHolderOne,
    PenHolderTwo,
    Bowl,
    Cup,
    PaperCup,
}