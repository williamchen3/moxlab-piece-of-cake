public enum GrabTypes
{
    Cones,
    SphereNormal,
    SphereLarge,
    Rectangle,
    Cylinder,
}