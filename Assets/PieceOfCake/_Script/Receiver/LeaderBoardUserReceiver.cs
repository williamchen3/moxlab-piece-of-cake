using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LeaderBoardUserReceiver : MonoBehaviour
{
    public TextMeshProUGUI TMP_Rank;
    public TextMeshProUGUI TMP_Name;
    public TextMeshProUGUI TMP_Socre;    


    // Start is called before the first frame update
    public void Init(int _rank, string _name, long _score)
    {
        TMP_Rank.text = _rank.ToString();
        TMP_Name.text = _name.ToString();
        TMP_Socre.text = _score.ToString();
    }

}
