using UnityEngine;

public enum SFXType
{
    Tap,
    Success,
    Result,
    Warning
}

public class SFXManager : MonoBehaviour
{
    public static SFXManager instance = null;

    public AudioSource SFXSource;

    public AudioClip Clip_Tap;
    public AudioClip Clip_Success;
    public AudioClip Clip_Result;
    public AudioClip Clip_Warning;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    public void PlayClip(SFXType mSFXType)
    { 
        switch (mSFXType)
        {
            case SFXType.Tap:
                SFXSource.PlayOneShot(Clip_Tap);
                break;
            case SFXType.Success:
                SFXSource.PlayOneShot(Clip_Success);
                break;
            case SFXType.Result:
                SFXSource.PlayOneShot(Clip_Result);
                break;
            case SFXType.Warning:
                SFXSource.PlayOneShot(Clip_Warning);
                break;
            default:
                break;
        }
    }

}
