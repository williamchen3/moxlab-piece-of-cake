using Oculus.Platform;
using Oculus.Platform.Models;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GetPlatformUsersHelper : MonoBehaviour
{
    public TextMeshPro UIText_UserName;
    public TextMeshPro UIText_DisplayName;

    private void Awake()
    {
        Core.Initialize();

        GetUserName();
    }

    private void GetUserName()
    {
        Users.GetLoggedInUser().OnComplete(GetLoggedInUserCallback);        
    }

    private void GetLoggedInUserCallback(Message msg)
    {
        if (!msg.IsError)
        {
            User user = msg.GetUser();
           
            string userName = user.ID.ToString();
            string displayName = user.DisplayName;

            UIText_UserName.text = userName;
            UIText_DisplayName.text = displayName;            

            Debug.LogWarning(userName);
            Debug.LogWarning(displayName);
        }
        else
        {
            UIText_UserName.text = "IsError";
        }
    }
}
